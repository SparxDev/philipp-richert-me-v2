import { ImpressumData, DatenschutzData } from "./legal-data";

const init = () => {
  initMarquee();

  document.getElementById('btn-impressum')?.addEventListener('click', createImpressumModal);
  document.getElementById('btn-datenschutz')?.addEventListener('click', createDatenschutzModal);
};

const initMarquee = () => {
  const source: HTMLElement | null = document.getElementById('skill-marquee-source');
  const clone: HTMLElement | null = document.getElementById('skill-marquee-clone');

  if (!source || !clone) return;

  source.childNodes.forEach(child => clone.appendChild(child.cloneNode(true)));
};

const createImpressumModal = () => {
  const content: HTMLElement = document.createElement('div');

  const title: HTMLHeadingElement = document.createElement('h1');
  title.textContent = 'Impressum';
  content.appendChild(title);

  const name: HTMLParagraphElement = document.createElement('p');
  name.textContent = ImpressumData.name;
  content.appendChild(name);

  const street: HTMLParagraphElement = document.createElement('p');
  street.textContent = ImpressumData.street;
  content.appendChild(street);
  
  const city: HTMLParagraphElement = document.createElement('p');
  city.textContent = ImpressumData.city;
  content.appendChild(city);
  
  createModal(content);
};

const createDatenschutzModal = () => {
  const content: HTMLElement = document.createElement('div');
  content.classList.add('datenschutz-content');

  const title: HTMLHeadingElement = document.createElement('h1');
  title.textContent = 'Datenschutzerklärung';
  content.appendChild(title);

  for (const item of DatenschutzData) {
    const element: HTMLElement = document.createElement(item.tag);
    
    if (typeof item.content === 'string')
      element.textContent = item.content;
    else
      for (const child of item.content) {
        const childElement: HTMLElement = document.createElement(child.tag);

        if (typeof child.content !== 'string') continue;

        childElement.textContent = child.content;
        element.appendChild(childElement);
      }

    content.appendChild(element);
  }

  const legalSource: HTMLParagraphElement = document.createElement('p');
  legalSource.textContent = 'Quelle: ';
  legalSource.classList.add('legal-source');
  const legalSourceLink: HTMLAnchorElement = document.createElement('a');
  legalSourceLink.textContent = 'https://www.e-recht24.de';
  legalSourceLink.href = 'https://www.e-recht24.de/';
  legalSourceLink.target = '_blank';
  legalSource.appendChild(legalSourceLink);
  content.appendChild(legalSource);
  
  createModal(content);
};

const createModal = (content: HTMLElement) => {
  const modalTarget: HTMLElement | null = document.getElementById('modal');
  const overlay: HTMLDivElement = document.createElement('div');
  const closeBtn: HTMLButtonElement = document.createElement('button');

  if (!modalTarget) return;

  overlay.classList.add('overlay');
  overlay.addEventListener('click', closeModalOnOverlay);

  const card: HTMLDivElement = document.createElement('div');
  card.classList.add('card');

  closeBtn.classList.add('modal-close-btn');
  closeBtn.addEventListener('click', closeModal);
  card.appendChild(closeBtn);

  card.appendChild(content);
  overlay.appendChild(card);
  modalTarget.appendChild(overlay);

  setTimeout(() => {
    overlay.style.opacity = '1';
  }, 0);
};

const closeModalOnOverlay = (e: MouseEvent) => {
  if (e.currentTarget !== e.target) return;

  closeModal();
};

const closeModal = () => {
  const modalTarget: HTMLElement | null = document.getElementById('modal');

  if (modalTarget) modalTarget.textContent = '';
}

init();
